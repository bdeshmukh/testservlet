package stepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import junit.framework.Assert;
import org.openqa.selenium.By;
import java.util.*;
import org.openqa.selenium.WebElement;

public class LoginStepDefinition {
	
	WebDriver driver;

	@Given("^user is already on Login Page$")
	public void user_is_already_on_Login_Page()  {

		 System.setProperty("webdriver.chrome.driver","C:\\tmp\\chromedriver\\chromedriver.exe");
		 driver = new ChromeDriver();
		 driver.get("http://localhost:8080/testServlet/index.jsp");
	}

	@When("^title of login page is google$")
	public void title_of_login_page_is_google()  {
	    String title = driver.getTitle();
		System.out.println("Title is "+ title);
		
				
	driver.findElement(By.name("no1")).sendKeys("12");
    driver.findElement(By.name("no2")).sendKeys("14");
    List<WebElement> radiobuttons = driver.findElements(By.name("r1"));
    for(WebElement radiobutton: radiobuttons) 
    { 
        if(radiobutton.getAttribute("value").equals("Mul"))
            radiobutton.click();
    }
    driver.findElement(By.name("submit")).click();
    String result = driver.findElement(By.xpath("//h1")).getAttribute("innerHTML");
    if(result.contains("168"))
    	System.out.println("Test Case passed");
    else
	    System.out.println("Test Case Failed");
	
	}

	/*
	@Then("^user enters username and password$")
	public void user_enters_username_and_password() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^user clicks on login button$")
	public void user_clicks_on_login_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^user is on home page$")
	public void user_is_on_home_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

*/

}
